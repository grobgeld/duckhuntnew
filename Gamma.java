//package hiddenmarkovmodel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alexhermansson
 */
public class Gamma {

    
    public static Object[] gamma (int N, double[][] A, double[][] B, double[][] alpha, double[][] beta, int[] O) {
        
        //int N = B.length; // Number of states
        //int M = B[0].length; // Number of possible observations
        int T = O.length;
        
        //Gamma and di_gamma
        double[][] gamma = new double[N][T];
        double[][][] di_gamma = new double[N][N][T];
        

        for (int t = 0; t < T-1; t++ ){
            double denominator = 0;
            for (int i = 0; i < N; i++){
                for (int j = 0; j < N; j++){
                    denominator += alpha[i][t] * A[i][j] * B[j][O[t+1]]*beta[j][t+1];
                    
                }
                
            }
            
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    di_gamma[i][j][t] = alpha[i][t] * A[i][j] * B[j][O[t+1]]*beta[j][t+1]/denominator;
                    gamma[i][t] += di_gamma[i][j][t];
                }
            }
            
        }
        
        // Special case for gamma[i][T-1]
        double denominator = 0;
        for (int i = 0; i < N; i++) {
            denominator += alpha[i][T-1];
        }
        
        for (int i = 0; i < N; i++) {
            gamma[i][T-1] = alpha[i][T-1]/denominator;
        }    
        
        
        return new Object[] {gamma, di_gamma};
    }
    
}
