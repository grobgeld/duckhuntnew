//package hiddenmarkovmodel;


import java.util.*;
import java.io.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alexhermansson
 */
public class AlphaBetaPass {

    
    public static Object[] alphaBetaC(int N, double[][] A, double[][] B, double[][] Pi, int[] O) {
        
        int T = O.length;
        //int N = B.length; // Number of states
        //int M = B[0].length; // Number of possible observations

        // Initialize alpha,beta and C
        double[][] alpha = new double[N][T];
        double[][] beta = new double[N][T];
        double[] C = new double [T];


        //Alpha Pass
        for (int i = 0; i < N; i++) {
            alpha[i][0] = Pi[0][i]*B[i][O[0]];
            C[0] += alpha[i][0];
        }

        // To avoid underflow we need to scale alpha
        if ( C[0] != 0) {
            C[0] = 1/C[0];
        }
        for (int i=0; i < N; i++){
            alpha[i][0] = C[0]*alpha[i][0];

        }


        // Calculate alpha iteratively for all t = 1, 2, .. , T
        for (int t = 1; t < T; t++) {
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    alpha[i][t] = alpha[i][t] + alpha[j][t-1]*A[j][i];
                }
                alpha[i][t] = alpha[i][t]*B[i][O[t]];
                C[t] += alpha[i][t];
            }

            // To vaid underflow we need to scale alpha
            if ( C[t] != 0) {
                C[t] = 1/C[t];
            }
            
            for (int i=0; i < N; i++){
                alpha[i][t] = C[t]*alpha[i][t];
            }
        }


        // BETA-pass


        // Scale beta[i][T-1]
        for (int i=0; i < N; i++){
            beta[i][T-1] = C[T-1];
        }

        // Calculate beta for t = 0 : T-1
        for (int t=T-2; t > -1; t--){
            for (int i=0; i < N; i++){
                for (int j=0; j < N; j++){
                    beta[i][t] += + A[i][j]*B[j][O[t+1]]*beta[j][t+1];
                }

                //Scaling beta[i][t]
                beta[i][t] = C[t]*beta[i][t];

            }
            
        }
        
            return new Object[] {alpha, beta, C};
    
    }

}
