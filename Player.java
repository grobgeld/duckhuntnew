import java.util.LinkedList;


class Player {
    
    int numSpecies = Constants.COUNT_SPECIES;
    int T = Constants.COUNT_MOVE;
    
    int timeLimit = 65;
    double shootLimit = 0.65;
    double storkLimit = 0.3;
    
    // Environment related
    BirdHMM[] models =  new BirdHMM[numSpecies];
    int[] totalSpeciesSeen;
    int totalBirdSeen = 0;
    double[] priorSpecies;
    
    //All observations for Species
    LinkedList<LinkedList<Integer>> totalObservations;
    
    //Constructor
    public Player() {
        
        //Initializing
    	totalObservations = new  LinkedList<LinkedList<Integer>>();
        totalSpeciesSeen = new int[numSpecies];
        priorSpecies = new double[numSpecies];
        
        //Initilize HMMs and total observations for every species 
         for (int i = 0; i < numSpecies; i++) {
        	models[i] = new BirdHMM(T); 
        	totalObservations.add(new LinkedList<Integer>());
         }
    
    }
    
      
    /**
     * Shoot!
     *
     * This is the function where you start your work.
     *
     * You will receive a variable pState, which contains information about all
     * birds, both dead and alive. Each bird contains all past moves.
     *
     * The state also contains the scores for all players and the number of
     * time steps elapsed since the last time this function was called.
     *
     * @param pState the GameState object with observations etc
     * @param pDue time before which we must have returned
     * @return the prediction of a bird we want to shoot at, or cDontShoot to pass
     */
    public Action shoot(GameState pState, Deadline pDue) {
        
        //Initilizing thing for every round
        int birdNumber = pState.getNumBirds();
        int birdToShoot = -1;
        int bestAction = -1;
        int round = pState.getRound();
        int timeStep = (pState.getBird(0)).getSeqLength();
        
        //Only start shooting after observing more than "timeLimit" time steps.
        if (timeStep < timeLimit) {
            return cDontShoot;
        }
        
        else {

            double bestProb = 0;
            
            // for every bird
            for (int i = 0; i < birdNumber; i++) { 

                Bird bird = pState.getBird(i);
                if (bird.isAlive()) {

                    //Get the observations for the bird and train an hmm for it
                    int[] obsArray = getObservationSeq(bird);
                    BirdHMM hmm = new BirdHMM(timeStep);
                    hmm.train(obsArray);

                    if (round > 0) {    

                            double[] probOfSpeciesArray = getMostLikelySpecies(bird);
                            double probOfStork = probOfSpeciesArray[Constants.SPECIES_BLACK_STORK];

                        if (probOfStork > storkLimit) {
                            
                            continue;
                        }
                    }

                    double[] probArray = hmm.probNextEmission();


                    for (int j = 0; j < Constants.COUNT_MOVE; j++) {
                        if (probArray[j] > bestProb) {
                            bestProb = probArray[j];
                            bestAction = j;
                            birdToShoot = i;
                        }

                    }    

                }

            }

            if (bestProb > shootLimit) {
                
                System.err.printf("Shooting bird %d (Action: %d) with prob %.5f\n", birdToShoot, bestAction,  bestProb);
                return new Action(birdToShoot, bestAction);
            }

            else {

                return cDontShoot;
            }
 
        }
        
    } //end of shoot
    
    
    //returns an array with probabilites for every species for bird
    public double[] getMostLikelySpecies(Bird bird) {
        
        double foundBestProb = 0;
        int[] obsArray = getObservationSeq(bird);
        double[] ProbArray = new double[numSpecies];
        
        //for every species model
        for (int i = 0; i < numSpecies; i++) {
            
            if (this.models[i].notInitialized()) {
                continue;
            }
            
            else {
                BirdHMM model = this.models[i];
                double probObsSeq = model.probObsSeq(obsArray);
                probObsSeq *= priorSpecies[i];
                
                ProbArray[i] = probObsSeq;
                
                if (probObsSeq > foundBestProb) {
                    foundBestProb = probObsSeq;
                }

            }
            
        } 
        
        //normalize to make it into actual probability
        ProbArray = normalize(ProbArray);
        
        return ProbArray;
        
    }
    
    //normalize a vector so the sum of the elements = 1
    public double[] normalize(double[] vector) {
    	
	    double[] normalizedVector = new double[vector.length];
	    double sum = 0.0;
	    for (int i=0; i<vector.length; i++){
		    sum += vector[i];
	    }
	    for (int i=0; i<vector.length; i++){
		    normalizedVector[i] = vector[i]/sum;
	    }
	    return normalizedVector;
    }
    
 
    /**
     * Guess the species!
     * This function will be called at the end of each round, to give you
     * a chance to identify the species of the birds for extra points.
     *
     * Fill the vector with guesses for the all birds.
     * Use SPECIES_UNKNOWN to avoid guessing.
     *
     * @param pState the GameState object with observations etc
     * @param pDue time before which we must have returned
     * @return a vector with guesses for all the birds
     */
    public int[] guess(GameState pState, Deadline pDue) {
        
        int round = pState.getRound();
        int bestSpecies;
        int[] lGuess = new int[pState.getNumBirds()];
        double bestProbSpecies;
        
        // for every bird
        for (int i = 0; i < pState.getNumBirds(); ++i) {
            
            Bird bird = pState.getBird(i);
            //On the first round, guess all pigeons.
            if (round == 0) {
                
                lGuess[i] = Constants.SPECIES_PIGEON;
            }
            //On all the other rounds
            else {
            	
                double[] probOfSpeciesArray = getMostLikelySpecies(bird);
                bestProbSpecies = 0;
                bestSpecies = 0;
                
                //checks probability for every species, saves the best one
            	for (int j = 0; j < probOfSpeciesArray.length; j++) {
            	
                    if (probOfSpeciesArray[j] > bestProbSpecies) {
                        
                        bestProbSpecies = probOfSpeciesArray[j];
                        bestSpecies = j;
            		}
                    
            	}
                
                lGuess[i] = bestSpecies;
                
            }
            
        }
        //show what we guess for
        System.err.print("Our guesses for round " + round);
        System.err.print(" are: ");
        for (int i = 0; i < lGuess.length; i++) {
            System.err.print(lGuess[i] + " ");
            
        }
        System.err.println();
        
        return lGuess;
    }
 
    /**
     * If you hit the bird you were trying to shoot, you will be notified
     * through this function.
     *
     * @param pState the GameState object with observations etc
     * @param pBird the bird you hit
     * @param pDue time before which we must have returned
     */
    public void hit(GameState pState, int pBird, Deadline pDue) {
        System.err.println("HIT BIRD!!!");
    }
 
    /**
     * If you made any guesses, you will find out the true species of those
     * birds through this function.
     *
     * @param pState the GameState object with observations etc
     * @param pSpecies the vector with species
     * @param pDue time before which we must have returned
     */
    public void reveal(GameState pState, int[] pSpecies, Deadline pDue) {
        
        
        // Prints actual species for this round
        System.err.print("Actual species: ");
            for (int j = 0; j < pSpecies.length; j++) {
                
                System.err.print(pSpecies[j] + " ");
            }  
        System.err.println();
        
        //for every bird, reveal and add model if the species have not been 
        //seen or retrain the model if the species have been seen before
        for (int i = 0; i < pSpecies.length; i ++) {
            Bird bird = pState.getBird(i);
            int[] observation = getObservationSeq(bird);
            int observationLength = observation.length;
            int species = pSpecies[i];
            int round = pState.getRound();
            
            totalSpeciesSeen[pSpecies[i]] +=1;
            
            for (int j=0; j < observationLength; j++) {
            	
                totalObservations.get(species).add(observation[j]);
            }
            
            
            int[] totalObservationsArray = toIntArray(totalObservations.get(species));
            
            //only train before round 8, after that it takes too much time and is unnecessary
            if (round <= 7) {
                
                models[species].train(totalObservationsArray);
            } 
            
        }
        
        totalBirdSeen += pSpecies.length;
        
        for (int i=0; i<6; i++) {
        	priorSpecies[i] = ((double) totalSpeciesSeen[i]) / ((double) totalBirdSeen);
        }
        
    } //end of reveal
 
    public static final Action cDontShoot = new Action(-1,-1);
    
    public int[] getObservationSeq(Bird bird){
        
        int seqLength = bird.getSeqLength();
        
        int[] observationSeq = new int[seqLength];
        
        for (int t = 0; t < seqLength; t++){
            
            if (bird.wasAlive(t)){
                observationSeq[t] = bird.getObservation(t);
            }
        }
        
        return observationSeq;
    }
    
    public int[] toIntArray(LinkedList<Integer> list) {
    	int[] array = new int[list.size()];
    	for (int i = 0; i<list.size(); i++) {
    		array[i] = list.get(i); 
    	}
    	
    	return array;
        
    }
    
}