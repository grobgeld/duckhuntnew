import java.util.HashMap;
 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
 
public class BirdHMM {
    
    //static constants
    public static final int N = 4; //4 gives best result
    public static final int M = 9;
    public static final int SPECIES = 6;
    public static final int maxIter = 6; //6 gives best result
    
    //instance variables needed for every object of type BirdHMM
    public int T;
    public double[][] gamma;
    public double[][] A;
    public double[][] B;
    public double[][] Pi;
    
    //"empty" constructor
    public BirdHMM() {
        this.T = 0;
    }
    
    //constructor to initialize A, B, Pi
    public BirdHMM(int T) {
        
        this.T = T;
        this.A = Matrix.rowStochasticMatrix(N,N);
        this.B = Matrix.rowStochasticMatrix(N,M);
        this.Pi = Matrix.rowStochasticMatrix(1,N);
   
    }
    
    //Check if BirdHMM has been initialized with a T
    public boolean notInitialized() {
        return (this.T == 0);
    }
    
    //trains the model with observationSeq (Baum Welch algorithm)
    public void train(int[] observationSeq) {
        
        //step 1 in Baum Welch
        T = observationSeq.length;
        double oldLogProb = Double.NEGATIVE_INFINITY;
        int iter = 0;
         
        double[][] newA = this.A;
        double[][] newB = this.B;
        double[][] newPi = this.Pi;
         
        while (iter < maxIter) {
            
            //step 2 and 3 in Baum Welch
            Object[] abcArray = AlphaBetaPass.alphaBetaC(N, newA, newB, newPi, observationSeq);
            double[][] alpha = (double[][])abcArray[0];
            double[][] beta = (double[][])abcArray[1];
            double[] C = (double[])abcArray[2];
 
            Object[] gammaArray = Gamma.gamma(N, newA, newB, alpha, beta, observationSeq);
            this.gamma = (double[][])gammaArray[0]; //save this for the model to use in probNextEmission
            double[][][] di_gamma = (double[][][])gammaArray[1];
             
            Object[] lambda = ReEstimate.lambda(di_gamma, this.gamma, observationSeq, N, M);
            newA = (double[][])lambda[0];
            newB = (double[][])lambda[1];
            newPi = (double[][])lambda[2];
            
            double logProb = 0;
            for (int i = 0; i < T; i++) {
                
                logProb += Math.log(C[i]);   
            }
            
            logProb = -logProb;
             
            iter++;
            if (iter < maxIter && logProb > oldLogProb) {
                
                oldLogProb = logProb;
            }
            
            else {
                
                break;
            }
        }    
         
        this.A = newA;
        this.B = newB;
        this.Pi = newPi;
         
    } //end of train
    
    
    //returns an array with the probability of next observation
    //res[i] = probability of doing move i.
    public double[] probNextEmission () {
        
        double[][] lastGamma = new double[1][N];
        
        for (int i = 0; i < N; i++) {
            
            lastGamma[0][i] = this.gamma[i][T-1];
        }
         
        double[][] temp = Matrix.multiply(lastGamma, this.A);
        double[][] resMatrix = Matrix.multiply(temp, this.B);
        double[] res = resMatrix[0];
        
        return res;
         
    } //end of probNextEmission
    
    //Gives the probability of observing observation sequence O with this model lambda.
    public double probObsSeq (int[] O) {
    	
        double probability = 0;
        double[][] alpha = new double[N][O.length];
        
        for (int i = 0; i < N; i++) {
            
            alpha[i][0] = this.Pi[0][i]*this.B[i][O[0]];
        }
        
        for (int t = 1; t < O.length; t++) {
            
            for (int i = 0; i < N; i++) {
                
                for (int j = 0; j < N; j++) {
                    
                    alpha[i][t] += alpha[j][t-1]*this.A[j][i]*this.B[i][O[t]];
                }
            }   
        }
        
        for (int i = 0; i < N; ++i) {
            probability += alpha[i][O.length-1];
        }
        
        return probability;
        
        
    } //end of probObsSeq
     
} //end of birdHMM class