//package hiddenmarkovmodel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.lang.Math;
import java.util.Random;

/**
 *
 * @author alexhermansson
 */
public class Matrix {
    
    public static double[] lineToArray(String[] string) {
        // Function taking a string, turning it into an array of doubles  
        double[] doubleArray = new double[string.length];
        for (int i = 0; i < string.length; i++) {
            String numberAsString = string[i];
            doubleArray[i] = Double.parseDouble(numberAsString);
        }
        
        return doubleArray;
    }
    
    public static int[] lineToIntArray(String[] string) {
        // Function taking a string, turning it into an array of ints  
        // Use for the observation O
        int[] intArray = new int[string.length];
        for (int i = 0; i < string.length; i++) {
            String numberAsString = string[i];
            intArray[i] = Integer.parseInt(numberAsString);
        }
        
        return intArray;
    }
    
    public static double[][] createMatrix(double[] vector) {
        /* Function taking a "vector" of form [N M elem00 elem01 ... elemNM]
           making it into an NxM matrix */
        int N = (int)vector[0];
        int M = (int)vector[1];
        double[][] matrix = new double[N][M];
        int i = 2;
        while(i < vector.length){
            for(int row = 0; row < N; row++){
                for(int col = 0; col < M; col++){
                    matrix[row][col] = vector[i];
                    i++;
                }
            }
        }
        return matrix;
    }
    
    public static double[][] rowStochasticMatrix(int N, int M) {
        // returns a matrix where all the rows are row stochastic
        
        double[][] matrix = new double[N][M];
        double eps = 0.01; //eps = 0.1 ---> score: 1 is best so far
        for (int i = 0; i < N; i++) {
            double sum = 0;
            for (int j = 0; j < M; j++) {
                matrix[i][j] = eps + Math.random();
                sum += matrix[i][j];
            }
            
            //normalize the row
            double c = 1/sum;
            for (int j = 0; j < M; j++) {
                matrix[i][j] *= c;
            }
        }
        return matrix;
        
    }
    
    public static void printMatrix(double[][] matrix) {
        
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.err.print(matrix[i][j] + " ");
            }
            System.err.println();
            
        }
        System.err.println();
        
    }
    
    public static void printMatrix(int[][] matrix) {
        
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.err.print(matrix[i][j] + " ");
            }
            System.err.println();
            
        }
        System.err.println();
        
    }
    
    public static void printMatrixAsLine(double[][] matrix) {
        
        System.out.print(matrix.length + " " + matrix[0].length);
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(" " + matrix[i][j]);
            }
        }
        System.out.println();
    }

    // return c = a * b
    public static double[][] multiply(double[][] a, double[][] b) {
        int m1 = a.length;
        int n1 = a[0].length;
        int m2 = b.length;
        int n2 = b[0].length;
        if (n1 != m2) throw new RuntimeException("Illegal matrix dimensions.");
        double[][] c = new double[m1][n2];
        for (int i = 0; i < m1; i++)
            for (int j = 0; j < n2; j++)
                for (int k = 0; k < n1; k++)
                    c[i][j] += a[i][k] * b[k][j];
        return c;
    }

    // matrix-vector multiplication (y = A * x)
    public static double[] multiply(double[][] a, double[] x) {
        int m = a.length;
        int n = a[0].length;
        if (x.length != n) throw new RuntimeException("Illegal matrix dimensions.");
        double[] y = new double[m];
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                y[i] += a[i][j] * x[j];
        return y;
    }


    // vector-matrix multiplication (y = x^T A)
    public static double[] multiply(double[] x, double[][] a) {
        int m = a.length;
        int n = a[0].length;
        if (x.length != m) throw new RuntimeException("Illegal matrix dimensions.");
        double[] y = new double[n];
        for (int j = 0; j < n; j++)
            for (int i = 0; i < m; i++)
                y[j] += a[i][j] * x[i];
        return y;
    }
    
}
